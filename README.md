# BHAX - A BEFEJEZETT Prog{1,2} könyv

## Fejezetek

### Prog1 **DONE**

Helló, Turing! -- **Done**

Helló, Chomsky! -- **Done**

Helló, Caesar! -- **Done**

Helló, Mandelbrot! -- **Done**

Helló, Welch! -- **Done**

Helló, Conway! -- **Done**

Helló, Schwarzenegger! -- **Done**

Helló, Chaitin! -- **Done**

Helló, Gutenberg! -- **Done**

**Befejezve: 2020. május 13.**

### Prog2 **Done**
Helló, Naplóm! -- **Done**

Helló, Arroway! -- **Done**

Helló, Liskov! -- **Done**

Helló, Mandelbrot! -- **Done**

Helló, Chomsky! -- **Done**

Helló, Stroustrup! -- **Done**

Helló, Gödel! -- **Done**

Helló, ! -- **Done**

Helló, Lauda! -- **Done**

Helló, Calvin! -- **Done**

**Befejezve: 2020. december 9.**
 