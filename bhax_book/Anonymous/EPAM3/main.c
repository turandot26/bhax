#include <stdio.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>

const char *argp_program_version = "Prog2-Caesar 0.0.1";
const char *argp_program_bug_address = "barthazoli@mailbox.unideb.hu";
static char doc[] = "A small commandline encoder program, that uses Caesar encoding.";
static char args_doc[] = " [OPTIONS]";
static struct argp_option options[] = {
	{ "user_input", 'u', 0, 0, "Wait input from the user."},
	{ "read_from_file", 'i', "InFile", 0, "Read the input from a file."},
	{ "write_to_stdout", 'v', 0, 0, "Write the output to stdout."},
	{ "write_to_file", 'o', "OutFile", 0, "Write the output to a file."},
	{ "offset", 'n', "Number", 0, "The offset of the encoding."},
	{ 0 }
};

struct arguments {
	enum { READ_FROM_STDIN, READ_FROM_FILE } input_mode;
	enum { WRITE_TO_STDOUT, WRITE_TO_FILE } output_mode;
	char *input_file;
	char *output_file;
	int enc_offset;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
	struct arguments *arguments = state->input;
	switch (key) {
		case 'u':
			arguments->input_mode = READ_FROM_STDIN;
			arguments->input_file = NULL;
			break;
		case 'i':
			arguments->input_mode = READ_FROM_FILE;
			arguments->input_file = arg;
			break;
		case 'v':
			arguments->output_mode = WRITE_TO_STDOUT;
			arguments->output_file = NULL;
			break;
		case 'o':
			arguments->output_mode = WRITE_TO_FILE;
			arguments->output_file = arg;
			break;
		case 'n':
			arguments->enc_offset = atoi(arg);
			break;
		case ARGP_KEY_ARG: return 0;
		default: return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

const int char_range = 'Z'-'A'+1;

char caesar_encode(char a, int offset)
{
	offset%=char_range;

	if(islower(a)){
		a+=offset;
		if(a > 'z')
			a = a - 'z' + 'a' - 1;
	} else if(isupper(a)){
		a+=offset;
		if(a > 'Z')
			a-=char_range;
			//a = a - 'Z' + 'A' - 1;

	}

	return a;

}

void print_arguments(struct arguments arguments)
{
	printf("offset: %d, input: %s, output: %s, \n",
		arguments.enc_offset,
		arguments.input_file ? arguments.input_file : "-",
		arguments.output_file ? arguments.output_file : "-");
}

int main(int argc, char **argv)
{
	struct arguments arguments;

	/* Default values. */
	arguments.input_mode = READ_FROM_STDIN;
	arguments.output_mode = WRITE_TO_STDOUT;
	arguments.input_file = NULL;
	arguments.output_file = NULL;
	arguments.enc_offset = 0;

	FILE *input = stdin;
	FILE *output = stdout;

	argp_parse (&argp, argc, argv, 0, 0, &arguments);

	if(arguments.enc_offset<0){
		printf("Error! Encoding offset can't be negative!");
		exit(727);
	}

	if(arguments.input_mode == READ_FROM_FILE)
		input = fopen(arguments.input_file, "r");
	else
		input = stdin;

	if (!input){
		printf("Couldn't open input file\n");
		exit(1);
	}

	if(arguments.output_mode == WRITE_TO_FILE)
		output = fopen(arguments.output_file, "w");
	else
		output = stdout;

	if (!output){
		printf("Couldn't open output file\n");
		exit(2);
	}

	char buffer;
	while ((buffer = fgetc(input)) != EOF)
		fputc(caesar_encode(buffer, arguments.enc_offset), output);

	fclose(input);
	fflush(output);
	fclose(output);

	exit (0);
}