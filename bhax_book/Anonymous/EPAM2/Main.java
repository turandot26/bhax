import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

public class Main {
    public static void main(String[] args) {

        if(args.length>2){
            System.out.println("Usage: java Main in_file [out_filename]");
            System.exit(1);
        }


        BufferedImage img = null;

        try {
            img = ImageIO.read(new File(args[0]));
        } catch (IOException e) {
            e.printStackTrace();
        }

        char[][] art = Art.imageToASCII(img);

        if(args.length == 2){
            try (PrintWriter out = new PrintWriter(args[1])) {
                for(char[] line : art)
                    out.println(line);
            } catch(IOException e) {
                e.printStackTrace();
            }
        }

        for (char[] line : art)
            System.out.println(line);
    }
}
