#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPooling2D

from PIL import Image
import numpy as np
import sys

physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
tf.config.experimental.set_memory_growth(physical_devices[0], True)

#kép beolvasása, átméretezése
input_image = np.array(Image.open(sys.argv[1]).convert('L').resize((28, 28), 0)).reshape(1, 28, 28, 1)

#adatbázis betöltése, méretre szabása
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()

x_train = x_train.reshape(x_train.shape[0], 28, 28, 1)
x_test  = x_test.reshape(x_test.shape[0], 28, 28, 1)
x_train = x_train.astype('float32')
x_test  = x_test.astype('float32')
x_train/= 255
x_test /= 255

#a neurális háló rétegei
model = Sequential()
model.add(Conv2D(28, kernel_size=(3,3), input_shape=(28, 28, 1)))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten())
model.add(Dense(128, activation=tf.nn.relu))
model.add(Dropout(0.2))
model.add(Dense(10,activation=tf.nn.softmax))

#neurális háló (súlyok) optimalizálása, loss függvény, hatékonyság megadása
model.compile(optimizer='adam', 
              loss='sparse_categorical_crossentropy', 
              metrics=['accuracy'])

#tanítási iterációk megadása
model.fit(x=x_train,y=y_train, epochs=10)

#kiértékelés
model.evaluate(x_test, y_test)

#kép átadása a feltanított neurális hálónak
pred = model.predict(input_image)

#kiíratás
print("The number is = ", pred.argmax())

