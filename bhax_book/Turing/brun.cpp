#include <iostream>

using namespace std;

int prime (int number)
{
	int number_of_divisors=0;
	for (int divisor=1; divisor<=number; divisor++)
	{
		if (number%divisor==0)
			{number_of_divisors++;}

	}
	if (number_of_divisors==2)
		{return 1;}
	else{return 0;};

}

int main (int argc, char* argv[])
{


	if(argc!=2){
	cout<<"Bad syntax" <<'\n' <<"How to use: $./brun limit_of_numbers" <<'\n';
		return 1;
	}

    int limit = stoi(argv[1]);
	float sum = 0;

	for (int index =1; index <= limit; index++ )
		{
			if(prime(index)==1&&prime(index+2)==1)
			{
				sum = sum + 1/float(index) + 1/float(index+2);
				cout << "\x1B[2J\x1B[H";
				cout<<index <<" " <<index+2 <<" --->>" <<sum <<'\n';

			}
		}
	return 0;

}