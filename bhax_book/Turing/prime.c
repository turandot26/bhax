#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define TRUE 1
#define FALSE 0

void push_prime(int num, int a, int* arr){
	arr =  realloc(arr, (a)*sizeof(int) );
	
	arr[a-1] = num;
	return;
}

int prime_check(int num, int a, int* arr){
	for(int i = 0; i < a; ++i){
		if(num%arr[i]==0){
			return FALSE;
		}
	}
	return TRUE;
}

void print_primes (int a, int* arr){
	for(int i = 0; i<a; i++){
		printf("%d ",arr[i] );
			if((i+1)%5==0){
				printf("\n");
			}
	}

	return;
}

int main(int argc, char** argv){
	
	if(argc!=2){
		printf("%s\n","Bad syntax or file not found" );
		return 1;
	}

	int end = atoi(argv[1]);
	int a = 0;
	int* primes = (int*) malloc(sizeof(int));
	primes[0] = 2;
	++a;
	double brun = 0;
	for(int i = 3; i<= end; i++){
		if(prime_check(i, a, primes)){
			++a;
			push_prime(i, a, primes);

		}

		if(prime_check(i+2, a, primes)){
			printf("%s\n","True" );
			brun = brun + 1/((double)i) + 1/((double)(i+2));
		}
	}
	print_primes(a, primes);
	printf("%f\n",brun );

	free(primes);
}