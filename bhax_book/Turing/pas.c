#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define BUFFER 100010
#define SIZE 20

int check_password(char text[])
{
    int i = 0;

    int low = 0;
    int upper = 0;
    int digit = 0;
    int special = 0;

    for(i = 0; i < strlen(text); i++)
    {
        if(text[i] >= 97 && text[i] <= 122)     //97 122
        {
            low++;
        }
        else if(text[i] >= 65 && text[i] <= 90)  //65 90
        {
            upper++;
        }
        else if(text[i] >= 48 && text[i] <= 57);  //48 57
        {
            digit++;
        }
        if(text[i]== 46 || text[i]== 44 || text[i]== 58 || text[i]== 59)
        {
            special++;
        }
    }

    if (low >= 1 &&  upper >= 1 && digit >= 2 && special >= 1) 
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int main()
{
    char* file_name = "passwords.txt";
    FILE *fp = fopen(file_name, "r");

    if(fp == NULL)
    {
        printf("Hiba! a fájl nem található!\n");
        exit(1);
    }

    char sor[BUFFER];
    int counter = 0;
    while(fgets(sor, BUFFER, fp) != NULL)
    {

        sor[BUFFER] = check_password(sor);
        if(check_password(sor) == 1)
        {
            counter++;
        }
    }
    fclose(fp);

    printf("%d\n", counter);
    return 0;

}