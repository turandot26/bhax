#include <iostream>
#include <vector>

using namespace std;
//az hogy a vektor méretét paraméterként átadom...
//ezt még a genfi egyezmény is tiltja
int prime_check(int num, int size, const vector<int> &elems){

	int a=0;
	bool prime = false;

	#pragma omp parallel
	    {
	#pragma omp for

   	for(int i = 0; i < size/2; i++){
		a++;
		if(num%elems[i]==0){
			prime = true;
			}
			
	        }
	    }

	cout<<"num of iterations: "<<a <<'\n';
	return prime;

}

void prime_print(int size, const vector<int> &elems){
	for(int i = 0; i < size; i++){
		cout<< elems[i];
		if((i+1)%5==0) cout << '\n';
	}
}

int main(int argc, char* argv[]){
	
	if(argc!=2){
		cout<<"Bad syntax" <<'\n' <<"How to use: $./primes limit_of_numbers" <<'\n';
		return 1;
	}

    int limit = stoi(argv[1]);

	vector <int> primes;
	primes.push_back(2);
	int vecsize = 1;
	double brun=0;

	for(int i = 3; i<limit; i++){
		if(prime_check(i, vecsize, primes)){
			primes.push_back(i);
			++vecsize;
		} else {
			continue;
		}
		if(prime_check(i+2, vecsize, primes)){
			brun = brun + (1/(double(i)))+(1/(double(i+2)));
			cout << "\x1B[2J\x1B[H";
			cout <<brun <<' ' <<i <<'/' <<limit <<'\n';
		}

	}

}
