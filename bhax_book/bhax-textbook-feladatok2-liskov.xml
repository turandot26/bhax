<chapter xmlns="http://docbook.org/ns/docbook"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Liskov!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>Liskov helyettesítés sértése</title>
        <para>
            Írjunk olyan OO, leforduló Java és C++ kódcsipetet, amely megsérti a Liskov elvet! Mutassunk rá a
megoldásra: jobb OO tervezés.
https://arato.inf.unideb.hu/batfai.norbert/PROG2/Prog2_1.pdf (93-99 fólia)
(számos példa szerepel az elv megsértésére az UDPROG repóban, lásd pl. source/binom/Batfai-
Barki/madarak/)
        </para>

        <para>Mi is a Liskov-elv?
        </para>
        <para>
                A SOLID elvek közül a Liskov elv. Jelentése a következő:
        </para>
        <para>
            Ha egy programban egy adott T altípusa S akkor
            minden olyan helyen ahol T használható, lecserélhető T
            S altípusával anélkül, hogy az hatással lenne a 
            program tulajdonságára.
        </para>
        <para>
            Azt már láttuk példákon keresztül, hogy bármilyen ős lecsélhető valamely gyermekébe,
            viszont ez a tény magában nem elégíti a Liskov-elvet, hiszen nincs garancia
            arra, hogy a program eredeti viselkedése/tulajdonsága megmarad.
        </para>
        <para>
            Legegyszerűbb példa az, ha azt mondjuk, hogy adott egy Rectangle
            osztály és annak gyermeke Square. Egy téglalap oldalainak hossza
            tetszőlegesen hosszú lehet, nem kötelező egyforma hosszúnak lenniük.
            A négyzet definíció szerint téglalap, viszont vannak megkötései,
            oldalainak hosszának meg kell egyeznie. Mi a Rectangle
            osztályunkban implementáltunk két olyan függvényt amik külön-külön
            módosítják a téglalap objektum szélességét/magasságát, a Square
            örökli ezeket a metódusokat, és láthatjuk már ebből előre, hogy megsértjük
            a Liskov-elvet, nem lesz használható azokon a helyeken a Square
            ahol a Rectangle használható volt.
        </para>
        <para>
            Tegyük fel, hogy van olyan függvény/metódus valahol a programunkban, aminek
            csak a téglalapunk szélességét van joga megváltoztatni. Ha a téglalap helyett
            argumentumként egy négyzetet adnánk meg, akkor annak magassága is változna, tehát
            a programunk helytelenül működne, megsértettük az Elvet.
        </para>
        <para>
            Akkor is megsértenénk az Elvet, ha 
            megvizsgálnánk, hogy milyen típusú a megadott argumentum, hiszen explicit
            megnéznénk, hogy elfogadható típusú objektumot kaptunk-e, miközben a feltétel
            az, hogy ahol megengedett egy szupertípus használata, ott az összes gyermekének
            is használhatónak kell lennie.
        </para>
        <programlisting language='Java'>
            <![CDATA[
		class Bird
{
        
    public void  fly()
    {
            System.out.println("Am flying...\n");
    }
}

class Eagle extends  Bird
{
    public void fly()
        {
        System.out.println("Eagle: flying..\n");
        }
}
class Penguin extends  Bird
{
}

class Liskov
{

    public static void flyBird(Bird b)
    {
        b.fly();
    }

    public static void main(String[] args)
    {
        Bird theEagle = new Eagle();
        Bird thePenguin = new Penguin();
        
        flyBird(theEagle);
        flyBird(thePenguin);
        
        
    }

}
         
			]]>
        </programlisting>

        <programlisting language='C++'>
            <![CDATA[
		#include <iostream>

class Bird
{
    
public:
   
    virtual void  fly()
    {
        std::cout << "Am flying...\n" ;
    }
};

class Eagle : public Bird
{
public:
  
    void fly() override
    {
        std::cout << "Eagle: flying..\n";
    }
};

class Penguin : public Bird
{
};

static void flyBird(Bird& b)
{
    b.fly();
}

int main()
{
    Eagle theEagle;
    Penguin thePenguin;
    
    flyBird(theEagle);
    flyBird(thePenguin);
    
    return 0;
}
        
			]]>
        </programlisting>

        <para>Egy példa arra hogy hogyan sértsük meg a Liskov-elvet. Mivel mindkét esetben az <literal>Eagle</literal> és a <literal>Penguin</literal> osztály is implementálta a <literal>Bird</literal> interfacet, azt gondolnánk, hogy ha meghívhatjuk a <function>flyBird</function> metódust, ami <literal>Bird</literal> objektumot vár paraméterként. Sajnos tévedtünk. Minkét esetben zátonyra fut a programunk, mivel a <literal>Penguin</literal> osztály nem implementálja a <literal>Bird</literal> interfész repülésért felelős metódusát.</para>

        <programlisting language='Java'>
            <![CDATA[
		interface Bird
{
    
}

interface IFlyingBird extends Bird
{
    public void  fly();
}
interface INotFlyingBird extends Bird
{
}
class Eagle implements IFlyingBird
{
    public void fly()
    {
        System.out.println("Eagle: flying..\n");
    }
}
class Penguin implements INotFlyingBird
{
}

class LiskovSub
{

    public static void flyBird(IFlyingBird b)
    {
        b.fly();
    }

    public static void main(String[] args)
    {
        Eagle theEagle = new Eagle();
        Penguin  thePenguin = new Penguin();
        
        flyBird(theEagle);
        //flyBird(thePenguin); //Fordítási hiba!!
        
        
    }

} 
         
			]]>
        </programlisting>

        <programlisting language='C++'>
            <![CDATA[
		#include <iostream>

class Bird
{
    
};

class IFlyingBird : public Bird
{
public:
    virtual void fly() = 0;
};
class INotFlyingBird : public Bird
{
};

class Eagle : public IFlyingBird
{
public:
  
    void fly() override
    {
        std::cout << "Eagle: flying..\n";
    }
};

class Penguin : public INotFlyingBird
{
};

static void flyBird(IFlyingBird& b)
{
    b.fly();
}

int main()
{
    Eagle theEagle;
    Penguin thePenguin;
    
    flyBird(theEagle);
    //flyBird(thePenguin); //Ez az utasításmár nem fog lefordulni...
    //A programtulajdonságai épek maradnak.
    
    return 0;
}
         
			]]>
        </programlisting>

        <para>Hogy ne csak ellenpéldát lássunk. Itt egy-egy interface, az <literal>IFlyingBird</literal> és a <literal>INotFlyingBird</literal> örököl a <literal>Bird</literal> interfacetől és a kiterjeszett interfacet implementálják az egyes madár osztályai. Ha olyan madáára (látsd: <literal>Penguin</literal>) hívjuk meg a <function>flyBird</function>, metódust, ami nem implementálja a <literal>IFlyingBird</literal> interfacet, az fordításidőben kiderül, a programunk le se fordul.</para>

        <figure>
            <title>Java: a rossz verzió runtime hibát dob</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Liskov/img/liskov_java.png" scale="40" />
                </imageobject>
                <textobject>
                    <phrase>Java: a rossz verzió runtime hibát dob</phrase>
                </textobject>
            </mediaobject>
        </figure>

        <figure>
            <title>C++: ez sajnos lefut, hiba nélkül, de nem jól</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Liskov/img/liskov_cpp.png" scale="40" />
                </imageobject>
                <textobject>
                    <phrase>C++: ez sajnos lefut, hiba nélkül, de nem jól</phrase>
                </textobject>
            </mediaobject>
        </figure>

        <para> Az előbbi programok a kivetkező módon fordíthatóak és futtathatóak.
        </para>
                        <programlisting language='bash'>
            <![CDATA[
		javac Liskov.java
        javac LiskovSub.java
        clang++ Liskov.cpp -o Liskov
        clang++ LiskovSub.cpp -o LiskovSub

        java Liskov
        java LiskovSub
        ./Liskov
        ./LiskovSub

			]]>
        </programlisting>
        <para>
            <link xlink:href="Liskov/Liskov_subs/Liskov.java">
                <filename>Liskov.java</filename>
            </link>
        </para>
        <para>
            <link xlink:href="Liskov/Liskov_subs/Liskov.cpp">
                <filename>Liskov.cpp</filename>
            </link>
        </para>
        <para>
            <link xlink:href="Liskov/Liskov_subs/LiskovSub.java">
                <filename>LiskovSub.java</filename>
            </link>
        </para>
        <para>
            <link xlink:href="Liskov/Liskov_subs/Liskov.cpp">
                <filename>LiskovSub.cpp</filename>
            </link>
        </para>
    </section>

    <section>
        <title>Szülő-gyerek</title>
        <para>
            Írjunk Szülő-gyerek Java és C++ osztálydefiníciót, amelyben demonstrálni tudjuk, hogy az ősön
keresztül csak az ős üzenetei küldhetőek!
Lásd fóliák! https://arato.inf.unideb.hu/batfai.norbert/PROG2/Prog2_1.pdf (98. fólia)
        </para>

        <para>
            Ez a feladat csupán azt demonstrálná, hogy nem lehetséges egy adott szülő referencián
            keresztül, ami egy gyerek objektumára hivatkozik, meghívni gyermeke egy olyan metódusát
            amit ő maga nem definiált.
        </para>
        <para>
            C++-ban, Javaban is, ezt polimorfizmussal tudjuk kimutatni, eleve polimorfizmusról
            beszélünk ha egy szülő mutató vagy referencia egy gyerekére mutat/hivatkozik.
        </para>
        <para>
            A nem Ősök által definiált metódusokhoz nem férhetünk hozzá,
            hacsak nem downcastoljuk az adott objektumot
            a tényleges típusára. Ez esetben viszont megsértjük az előző feladatban ismertetett
            Liskov-elvet.
        </para>
        <programlisting language='Java'>
            <![CDATA[
		class Parent
{
    public void saySomething()
    {
        System.out.println("Parent says something");
    }
}
class Child extends Parent
{
    public void echoSomething(String msg)
    {
        System.out.println(msg);
    }
}
public class App
{
    public static void main(String[] args)
    {
        Parent p = new Parent();
        Parent p2 = new Child();
        
        System.out.println("Invoking method of parent");
        p.saySomething();
        
        System.out.println("Invoking method of child through parent ref");
        p2.echoSomething("This won't work");
        
    }
}
         
			]]>
        </programlisting>

        <programlisting language='C++'>
            <![CDATA[
		#include <iostream>
#include <string>

class Parent
{
public:
    void saySomething(){
        std::cout << "Parent says something\n";
    }
};

class Child : public Parent
{
public:
    void echoSomething(std::string msg){
       std::cout << msg << "\n";
    }
};


int main(){
        Parent* p = new Parent();
        Parent* p2 = new Child();
        
        std::cout << "Invoking method of parent\n";
        p->saySomething();
        
        std::cout << "Invoking method of child through parent ref\n";
        p2->echoSomething("This won't work");
        
        delete p;
        delete p2;
        
}         
			]]>
        </programlisting>
        <para>Az alábbi módon fordíthatóak és (amennyiben orvosoljuk hibákat) futtathatóak.</para>
                <programlisting language='bash'>
            <![CDATA[
		javac App.java
        clang++ App.cpp -o App
        # Ha eltávolítjuk a gyermek felé a hívásokat, akkor fordíthatóak és futtathatóak.
        # java App
        # ./App
			]]>
        </programlisting>

        <figure>
            <title>Java: Something's wrong, I can feel it...</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Liskov/img/parent1.png" scale="40" />
                </imageobject>
                <textobject>
                    <phrase>Java: Something's wrong, I can feel it...</phrase>
                </textobject>
            </mediaobject>
        </figure>

        <figure>
            <title>C++: még mindig inkább ez, mint egy 1200 soros template error</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Liskov/img/parent2.png" scale="40" />
                </imageobject>
                <textobject>
                    <phrase>C++: még mindig inkább ez, mint egy 1200 soros template error</phrase>
                </textobject>
            </mediaobject>
        </figure>

        <para>Mindkét esetben már compile-time alatt kiderült, hogy hibát vétettünk. Azt, hogy miért nem hívható a <literal>Child</literal> metódusa a <literal>Parent</literal>-ből, azt a következő képpen is magyarázhatjuk. Minden amit egy osztályban elhelyezünk, változók, adatok, még a metódusok is, valahol replrezentálva lesznek a memóriában. Ha egy soztály örököl egy másiktól, akkor a szülő osztály tagjainak is kell foglalni memóriát. Ha egy <literal>Parent</literal> típusú pointert/referenciát hozunk létre, akkor az csak azokata tagokat fogja "látni", amiknek hely foglalódik egy <literal>Parent</literal> típusú objektum létrehozásakor. Miért is lenne másképp? Mivel a <literal>Child</literal> típusú objektumok goflalnak memóriát a <literal>Parent</literal> tagjainak is, így őket nyugodt szívvel hívhatjuk; azonabn a <literal>Parent</literal> típusú pointer nem "látja" a <literal>Child</literal> tagjait, így a compiler számára e teljesen értelmetlen utasítás.
        </para>

        <para>
            <link xlink:href="Liskov/Parent/App.cpp">
                <filename>App.cpp</filename>
            </link>
        </para>

        <para>
            <link xlink:href="Liskov/Parent/App.java">
                <filename>App.java</filename>
            </link>
        </para>
    </section>

    <section>
        <title>Anti OO</title>
        <para>
            A BBP algoritmussal 5 a Pi hexadecimális kifejtésének a 0. pozíciótól számított 10e6, 10e7, 10e8 darab
jegyét határozzuk meg C, C++, Java és C# nyelveken és vessük össze a futási időket!
https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/apas03.html#id561066</para>
        <para>Avagy hogyan szerettem bele a Make-be és a shell scriptekbe...</para>
        <para>A kódon, tekintve hogy nem túlságosan mélyen objektum orientált, csak kisebb változtatásokat kell eszközölnünk, hogy más nyelvekre portoljuk, főleg hogy a 3 új nyelvből 2 támogatja az OOP-t, a harmadik meg C.</para>

                <programlisting language='C'>
            <![CDATA[
		#include <stdio.h>
#include <stdlib.h>
#include <math.h>

char hexLetters[] = {'A','B','C','D','E', 'F'};

long nMod(int n, int k)
{
		int t = 1;
		
		while(t <= n)
		{
			t = 2*t;
			
		}
		long r = 1;
		
		while(1)
		{
			if(n >= t)
			{
				r = 16*r % k;
				n = n - t;
			}
			t/=2;
			if(t < 1)
				break;
			if(t>=1)
			{
				r = r*r % k;
			}
		}
		
		return r;
		
}

double getS(int d, int j)
{
		double ret = 0.0;
		
		for(int i=0; i<d; i++)
		{
			ret+= (nMod(d-i, 8*i+j))/(double)(8*i+j);
			
		}
		
		return ret - floor(ret);
}
void calculate(int n)
{   
    double pi = 0.0;
    double pi1 = 4* getS(n,1);
    double pi2 = 2*getS(n,4);
    double pi3 = getS(n,5);
    double pi4 = getS(n,6);
    
    pi = pi1-pi2-pi3-pi4;
    pi = pi-floor(pi);
    int hex = floor(16.0*pi);
    printf("%c\n",((hex < 10) ? (char)((hex) + '0') : hexLetters[hex-10]));   
}
int main(int argc, char* argv[])
{
        if(argc == 1)
        {
            printf("Usage: ./BPP <exponent>\n");
            exit(-1);
        }
        int exponent = atoi(argv[1]);
        if(!exponent)
        {
            printf("Usage: ./BPP <exponent>\n");
            exit(-1);
        }
        printf("10^%d\n", exponent);
        int limit = pow(10,exponent);
        calculate(limit);				
		
}
         
			]]>
        </programlisting>

                <programlisting language='C++'>
            <![CDATA[
		#include <iostream>
#include <string>
#include <cmath>

char hexLetters[] = {'A','B','C','D','E', 'F'};

long nMod(int n, int k)
{
    int t = 1;
    
    while(t <= n)
    {
        t = 2*t;
        
    }
    long r = 1;
    
    while(true)
    {
        if(n >= t)
        {
            r = 16*r % k;
            n = n - t;
        }
        t/=2;
        if(t < 1)
            break;
        if(t>=1)
        {
            r = r*r % k;
        }
    }
    
    return r;
    
}
double getS(int d, int j)
{
    double ret = 0.0;
    
    for(int i=0; i<d; i++)
    {
        ret+= (nMod(d-i, 8*i+j))/(double)(8*i+j);
        
    }
    
    return ret - std::floor(ret);
}
void calculate(int n)
{
    double pi = 0.0;
    double pi1 = 4* getS(n,1);
    double pi2 = 2*getS(n,4);
    double pi3 = getS(n,5);
    double pi4 = getS(n,6);
    
    pi = pi1-pi2-pi3-pi4;
    pi = pi-std::floor(pi);
    
    int hex = std::floor(16.0*pi);
    std::cout << ((hex < 10) ? (char)(hex + '0') : hexLetters[hex-10]);
}
int main(int argc, char* argv[])
{
    if(argc == 1)
    {
        std::cout <<"Usage: ./BPP <exponent>\n";
        exit(-1);
    }
    int exponent = std::atoi(argv[1]);
    if(!exponent)
    {
        std::cout << "Usage: ./BPP <exponent>\n";
        exit(-1);
    }
    std::cout << "10^" << exponent << "\n";
    long long limit = std::pow(10,exponent);
    
    calculate(limit);
    
    std::cout << std::endl;
	return 0;	
		
}
        
			]]>
        </programlisting>

                <programlisting language="c++">
            <![CDATA[using System;

namespace BPP
{
    class Program
    {
        public static double getS(int d, int j) 
        {
            
            double ret = 0.0d;
            
            for(int k=0; k<=d; ++k)
                ret += (double)nMod(d-k, 8*k + j) / (double)(8*k + j);
            
            
            return ret - Math.Floor(ret);
        }
    
        public static long nMod(int n, int k) 
        {
            
            int t = 1;
            while(t <= n)
                t *= 2;
            
            long r = 1;
            
            while(true) {
                
                if(n >= t) {
                    r = (16*r) % k;
                    n = n - t;
                }
                
                t = t/2;
                
                if(t < 1)
                    break;
                
                r = (r*r) % k;
                
            }
            
            return r;
        }
        public static void calculate(int n)
        {
            char[] hexLetters = {'A','B','C','D','E','F'};
            double pi = 0.0;
            double pi1 = 4* getS(n,1);
            double pi2 = 2*getS(n,4);
            double pi3 = getS(n,5);
            double pi4 = getS(n,6);
            
            pi = pi1-pi2-pi3-pi4;
            pi = pi-Math.Floor(pi);
            int hex = (int)Math.Floor(16.0*pi);
            Console.WriteLine("A {0}. pozíción lévő jegye {1}",n, ((hex < 10) ? (char)((hex) + '0') : hexLetters[hex-10]));
        }
    
        public static void Main(String[] args)
        { 
            if(args.Length == 0)
            {
                Console.WriteLine("Usage: dotnet run <exponent>");
                Environment.Exit(-1);
            }
            try
            {
                Console.WriteLine("10^" + Convert.ToInt32(args[0]));
                calculate((int)Math.Pow(10,Convert.ToInt32(args[0])));
            }
            catch (FormatException e)
            {
                Console.WriteLine(e);
                Console.WriteLine("Usage: dotnet run <exponent>");
                Environment.Exit(-1);
            }
           
        }
            
    }
}
        
			]]>
        </programlisting>
    
                <programlisting language='Make'>
            <![CDATA[
		all: BPP.java BPP.c BPP.cpp
	javac BPP.java
	g++ -o BPP_PLUS BPP.cpp -O3
	gcc -o BPP BPP.c -lm -O3
         
			]]>
        </programlisting>

        <para>A fordítást (a C# kivételével) egy Makefile-ra bíztam. A Java runtime optimalizálva van (a Just-In-Time compile egyik előnye), ezért lelkiismeretfurdalás nélkül engedem rá a C/C++ compilerrekre az O3 optimalizációs flaget. Illetve ebben a példában nem clang/clang++ használok compilerként, hanem GNU szoftvereket, mivel ezek (a fáma szerint), gyorsabb bináris fájlokat eredményeznek (gyakorlatban csak marginális a különbség, vagy egyáltalán nincs is).</para>

                        <programlisting language='Bash'>
            <![CDATA[
		#!/bin/zsh

exponents=(6 7 8)

echo "A BBP algoritmus mérése a következő gépen:"
neofetch

echo "JAVA program mérési eredményei"

for exponent in ${exponents[*]}
do
	time java BPP $((exponent))
	echo
done	

echo "C program mérési eredményei"

for exponent in ${exponents[*]}
do
	time ./BPP $((exponent))
	echo
done

echo "C++ program mérési eredményei"

for exponent in ${exponents[*]}
do
	time ./BPP_PLUS $((exponent))
	echo
done

echo "C# program mérési eredményei"
for exponent in ${exponents[*]}
do
    cd CSHARP
	time dotnet run $((exponent))
	echo
	cd ..
done


echo "Teszt sikeresen végrehajtva!"
      
			]]>
        </programlisting>

        <para>A tesztelést egy shell script-re bíztam, gyakorlatilag minden program esetében végigiterál a három kitevőn és futási időt kitolja az STDOUT-ra.
        </para>
    <para>A C# project létrehozásához több parancsra is szükségünk van, sajnos nem működik a <code><![CDATA[touch filename.ext]]></code> -->> (programmer magic) -->> (compiling) -->> (run) alakú, bevett rutin.
    </para>
    <para>Létrehozáshoz, fordításhoz és futtatáshoz az alábbi parancsok szükségesek:
    </para>
    <programlisting language='Java'>
            <![CDATA[
		dotnet new console --name BPP
        dotnet build
        dotnet run         
			]]>
        </programlisting>

                		<figure>
            <title>A teszt rendszer</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Liskov/img/sys.png" scale="40" />
                </imageobject>
                <textobject>
                    <phrase>frázis</phrase>
                </textobject>
            </mediaobject>
        </figure>

                <programlisting language='bash'>
            <![CDATA[
		JAVA program mérési eredményei
10^6
java BPP $((exponent))  2,35s user 0,08s system 60% cpu 4,039 total
10^7
java BPP $((exponent))  25,41s user 0,02s system 99% cpu 25,509 total
10^8
java BPP $((exponent))  297,75s user 0,96s system 99% cpu 5:00,92 total

C program mérési eredményei
10^6
./BPP $((exponent))  2,08s user 0,02s system 97% cpu 2,147 total
10^7
./BPP $((exponent))  24,64s user 0,04s system 99% cpu 24,902 total
10^8
./BPP $((exponent))  282,30s user 0,35s system 99% cpu 4:44,07 total

C++ program mérési eredményei
10^6
./BPP_PLUS $((exponent))  2,10s user 0,00s system 97% cpu 2,151 total
10^7
./BPP_PLUS $((exponent))  24,34s user 0,02s system 99% cpu 24,481 total
10^8
./BPP_PLUS $((exponent))  282,40s user 0,58s system 99% cpu 4:44,43 total

C# program mérési eredményei
10^6
dotnet run $((exponent))  5,79s user 0,48s system 55% cpu 11,345 total
10^7
dotnet run $((exponent))  32,73s user 0,30s system 102% cpu 32,335 total
10^8
dotnet run $((exponent))  355,09s user 1,06s system 99% cpu 5:58,12 total

Teszt sikeresen végrehajtva!         
			]]>
        </programlisting>

        <para>Nos.. az eredmények egyrészt meglepőek, másrészt várhatóak, harmadrést, megmagyarázhatóak.
        </para>

        <para>A C és a C++ kb. ugyanolyan jól teljesítettek. Ez annyira nem meglepő. Még mindig a két goto low-level high-level nyelv (a hardver felett 3 méterrel), és ha tippelni kéne, a genrált assembly is hasonlóan néz ki. Ami meglepőbb, hogy a Java kitartóan tartotta a C és a C++ ütemét. A JIT optimization megtette a hatását; bravó Java, bár a nyelv runtime-costly részét sajnos nem használtuk és a Java-t eddig nem nagyon láttam apró, CLI programokban használva. Eltekintve attól a ténytől, hogy nem a súlycsoportjában indult, szépen teljesített. Nade... a C#. Igen, csúnyán alultejesített. DE. A .NET framework még nem túl kiforrott Linuxon, így ezt írjuk az enyhítő körülmények közé. 
        </para>
        <para>Konklúzió: egy programot rengeteg faktor lassíthat be. 
        </para>
        		<itemizedlist>
			<listitem>
				<para>A nyelv runtime nehéz.
				</para>
			</listitem>
    			<listitem>
				<para>Rossz tervezés
				</para>
			</listitem>

    			<listitem>
				<para>Rendszerhívások.
				</para>
			</listitem>

			<listitem>
				<para>A nyelv olyan funkcióját használjuk, ami rengetek overheaddel jár.
				</para>
			</listitem>

            <listitem>
				<para>Más nyelvet kéne használnunk (rád kacsintgatok Julialang)
				</para>
			</listitem>
		</itemizedlist>

        <para>Nincs rossz nyelv (még a Javascript is szerethető... minden február 30-án) csak a fentebbi felsorolt hibák. Egyértelműen nem használtuk ki a Java OOP funkcióit, de legalább nem volt útban az OOP.
        </para>

                		<para>
            <link xlink:href="Liskov/AntiOOP/BPP.c">
                <filename>BPP.c</filename>
            </link>
        </para> 

        		<para>
            <link xlink:href="Liskov/AntiOOP/BPP.cpp">
                <filename>BPP.cpp</filename>
            </link>
        </para> 

        		<para>
            <link xlink:href="Liskov/AntiOOP/BPP.java">
                <filename>BPP.java</filename>
            </link>
        </para> 

        		<para>
            <link xlink:href="Liskov/AntiOOP/cs/BPP/BPP.cs">
                <filename>BPP.cs</filename>
            </link>
        </para> 
    </section>

    <section>
        <title>EPAM: Interfész evolúció Java-ban</title>
        <para>
            Mutasd be milyen változások történtek Java 7 és Java 8 között az interfészekben. Miért volt erre
szükség, milyen problémát vezetett ez be?.</para>
        <para>Java 8 előtt az interfacek alapvetően csak publikus és abstract metódusokat tartalmaztak. Java 8 után már azonban tartalmazhattak statikus és default metódusokat
        </para>
        <para>Hogy minek default metódusok? Az Oracle szeretett volna egy olyan funkciót hozzáadni a Javahoz, ami lehetővé teszi azt, hogy a fejlesztők hozzáadjanak az interfacekhez olyan metódusokat, anélkül, hogy ez hatással lenne az interfacet implementáló osztályokra.
        </para>
        <para>A default metódusok implementálása opcionális az interfacet implementáló osztályokban. Ha egy interfacet 3 osztály implementál akkor ez lehet hogy nem tűnik akkor dealnek, deha már többszáz osztály implementálja, akkor ez igen nagy deal.
        </para>

        <programlisting language='Java'>
            <![CDATA[
		    interface MyInterface{  
 
        default void newMethod(){  
            System.out.println("Newly added default method for MyInterface");  
        }  
        void existingMethod(String str);  
    }  
    interface MyInterface2{  
         
        default void newMethod(){  
            System.out.println("Newly added default method for MyInterface2");  
        }  
        void disp(String str);  
    } 
    public class Bad implements MyInterface, MyInterface2{ 

        public void existingMethod(String str){           
            System.out.println("String is: "+str);  
        }  
        public void disp(String str){
            System.out.println("String is: "+str); 
        }
        
        public static void main(String[] args) {  
            Example obj = new Example();

            obj.newMethod();     
      
      
        }  
    }        
			]]>
        </programlisting>

        <para>Hol az a bizonyos bökkenő?
        </para>
        <para>Nos, ott, hogy amikor két intarfacenek van egy-egy ugyanolyan függvény szignatúrájú default metódusa, akkor az implementáló osztályból ezt meghívva (feltéve hogy nem implementálja ezt a fv-t), compiler errort kapunk, mivel nem tudjuk eldönteni, melyik fv-t hívjuk meg. 
        </para>
        <para>Akkor mi legyen a megoldás?
        </para>
        <para>Implementáljuk a duplikátum függvényt az implementáló osztályban!
        </para>

                <programlisting language='Java'>
            <![CDATA[
		    interface MyInterface{  
 
        default void newMethod(){  
            System.out.println("Newly added default method for MyInterface");  
        }  
        void existingMethod(String str);  
    }  
    interface MyInterface2{  
         
        default void newMethod(){  
            System.out.println("Newly added default method for MyInterface2");  
        }  
        void disp(String str);  
    } 
    public class Good implements MyInterface, MyInterface2{ 

        public void existingMethod(String str){           
            System.out.println("String is: "+str);  
        }  
        public void disp(String str){
            System.out.println("String is: "+str); 
        }
        @Override
        public void newMethod(){  
            System.out.println("Newly added implementation for the default methods of MyInterface and  MyInterface2");  
        }  
        
        public static void main(String[] args) {  
            Example obj = new Example();

            obj.newMethod();     
      
      
        }  
    }         
			]]>
        </programlisting>

        		<figure>
            <title>Le se fordul</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Liskov/img/epam1.png" scale="40" />
                </imageobject>
                <textobject>
                    <phrase>frázis</phrase>
                </textobject>
            </mediaobject>
        </figure>

        		<figure>
            <title>És működik</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Liskov/img/epam2.png" scale="40" />
                </imageobject>
                <textobject>
                    <phrase>frázis</phrase>
                </textobject>
            </mediaobject>
        </figure>

        		<para>
            <link xlink:href="Liskov/Epam1/Good.java">
                <filename>Good.java</filename>
            </link>
        </para> 

        		<para>
            <link xlink:href="Liskov/Epam1/Bad.java">
                <filename>Bad.java</filename>
            </link>
        </para> 


    </section>



</chapter>                
