#include <iostream>  
#include <string>
  
class Obj {  
    int* data;
public: 
    Obj(int i) {
        std::cout<<"Constructor\n";
        data = new int(i);
    }  

    ~Obj() {
        std::string val;
        if(data)
            val = std::to_string(*data);
        else
            val = "nullptr";
            
        std::cout<<"Destructor called with value "<< val <<"\n";
        delete data;
    }
  
    Obj(const Obj &p) : data (new int) {
        std::cout<<"Copy constructor\n";  
        *this->data=*(p.data);
    }  
      
    Obj& operator = (const Obj &t) { 
        std::cout<<"Copy assignement\n"; 
        int *tmp = new int();
        *tmp = *(t.data);
        delete data;
        data = tmp;
        return *this; 
    }  

    Obj (Obj && t) : data (nullptr) {
        std::cout<<"Move constructor\n";
        *this = std::move(t);
    }

    Obj& operator = (Obj && t){
        std::cout<<"Move assignement\n";
        std::swap(data,t.data);
        return *this;
    }
    
    void printObj(){
        std::cout <<"<<Value is:>> ";
        if(data)
            std::cout << *data <<'\n';
        else
            std::cout << "Empty\n";
    }
};

void br(){
    std::cout<<"\n-------------\n";
}
  
int main(){
    std::cout<<"Calling -- constructor\n";
    Obj obj1(7);
    obj1.printObj();
    
    br();
    
    std::cout<<"Calling -- copy constructor\n";
    Obj obj2(obj1);
    obj1.printObj();
    obj2.printObj();
    
    br();

    std::cout<<"Calling -- moving constructor\n";
    Obj obj3 = std::move(obj2);
    obj1.printObj();
    obj2.printObj();
    obj3.printObj();
    
    br();

    std::cout<<"Calling -- copy assignement\n";
    Obj obj4 = obj1;
    obj1.printObj();
    obj2.printObj();
    obj3.printObj();
    obj4.printObj();
    
    br();

    std::cout<<"Calling -- constructor\n";
    Obj obj5(27);
    obj1.printObj();
    obj2.printObj();
    obj3.printObj();
    obj4.printObj();
    obj5.printObj();
    
    br();
    
    std::cout<<"Calling -- move assignement\n";
    obj5 = std::move(obj1);
    obj1.printObj();
    obj2.printObj();
    obj3.printObj();
    obj4.printObj();
    obj5.printObj();
    
    br();

      
    return 0;  
}  
