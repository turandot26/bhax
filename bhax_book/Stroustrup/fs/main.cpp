#include <string>
#include <iostream>
#include <filesystem>
#include <algorithm>

void tabulate(std::string s){
	size_t n = std::count(s.begin(), s.end(), '/');

	for(size_t i = 1; i<n; ++i)
		std::cout<<"  ";
}

int main(int argc, char* argv[]){
	std::string pathString (argv[1]);
    std::filesystem::path path = pathString;
    for (auto const& file : std::filesystem::recursive_directory_iterator(path))
        if (std::filesystem::path(file.path()).extension() == ".java" &&std::filesystem::is_regular_file(file.path())){
			tabulate(file.path());
        	std::cout << file.path() << std::endl;
        }

    return 0;
}
