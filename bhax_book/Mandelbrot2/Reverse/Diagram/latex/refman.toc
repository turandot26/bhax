\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {chapter}{\numberline {1}Class Index}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Class List}{1}{section.1.1}%
\contentsline {chapter}{\numberline {2}File Index}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}File List}{3}{section.2.1}%
\contentsline {chapter}{\numberline {3}Class Documentation}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}L\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Z\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}W\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Bin\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Fa Class Reference}{5}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Constructor \& Destructor Documentation}{6}{subsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.1.1}LZWBinFa()}{6}{subsubsection.3.1.1.1}%
\contentsline {subsubsection}{\numberline {3.1.1.2}$\sim $LZWBinFa()}{6}{subsubsection.3.1.1.2}%
\contentsline {subsection}{\numberline {3.1.2}Member Function Documentation}{6}{subsection.3.1.2}%
\contentsline {subsubsection}{\numberline {3.1.2.1}getAtlag()}{7}{subsubsection.3.1.2.1}%
\contentsline {subsubsection}{\numberline {3.1.2.2}getMelyseg()}{7}{subsubsection.3.1.2.2}%
\contentsline {subsubsection}{\numberline {3.1.2.3}getSzoras()}{7}{subsubsection.3.1.2.3}%
\contentsline {subsubsection}{\numberline {3.1.2.4}kiir()\hspace {0.1cm}{\footnotesize \ttfamily [1/2]}}{7}{subsubsection.3.1.2.4}%
\contentsline {subsubsection}{\numberline {3.1.2.5}kiir()\hspace {0.1cm}{\footnotesize \ttfamily [2/2]}}{7}{subsubsection.3.1.2.5}%
\contentsline {subsubsection}{\numberline {3.1.2.6}operator$<$$<$()}{7}{subsubsection.3.1.2.6}%
\contentsline {subsubsection}{\numberline {3.1.2.7}ratlag()}{7}{subsubsection.3.1.2.7}%
\contentsline {subsubsection}{\numberline {3.1.2.8}rmelyseg()}{8}{subsubsection.3.1.2.8}%
\contentsline {subsubsection}{\numberline {3.1.2.9}rszoras()}{8}{subsubsection.3.1.2.9}%
\contentsline {subsection}{\numberline {3.1.3}Friends And Related Function Documentation}{8}{subsection.3.1.3}%
\contentsline {subsubsection}{\numberline {3.1.3.1}operator$<$$<$}{8}{subsubsection.3.1.3.1}%
\contentsline {subsection}{\numberline {3.1.4}Member Data Documentation}{8}{subsection.3.1.4}%
\contentsline {subsubsection}{\numberline {3.1.4.1}atlag}{8}{subsubsection.3.1.4.1}%
\contentsline {subsubsection}{\numberline {3.1.4.2}gyoker}{8}{subsubsection.3.1.4.2}%
\contentsline {subsubsection}{\numberline {3.1.4.3}maxMelyseg}{8}{subsubsection.3.1.4.3}%
\contentsline {subsubsection}{\numberline {3.1.4.4}szoras}{8}{subsubsection.3.1.4.4}%
\contentsline {chapter}{\numberline {4}File Documentation}{9}{chapter.4}%
\contentsline {section}{\numberline {4.1}log.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}txt File Reference}{9}{section.4.1}%
\contentsline {section}{\numberline {4.2}lzw.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}cpp File Reference}{9}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Function Documentation}{10}{subsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.1.1}main()}{10}{subsubsection.4.2.1.1}%
\contentsline {subsubsection}{\numberline {4.2.1.2}usage()}{10}{subsubsection.4.2.1.2}%
\contentsline {chapter}{Index}{11}{section*.9}%
