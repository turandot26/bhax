\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {chapter}{\numberline {1}Hierarchical Index}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Class Hierarchy}{1}{section.1.1}%
\contentsline {chapter}{\numberline {2}Class Index}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Class List}{3}{section.2.1}%
\contentsline {chapter}{\numberline {3}File Index}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}File List}{5}{section.3.1}%
\contentsline {chapter}{\numberline {4}Class Documentation}{7}{chapter.4}%
\contentsline {section}{\numberline {4.1}Bird Class Reference}{7}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Member Function Documentation}{8}{subsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.1.1}walkAround()}{8}{subsubsection.4.1.1.1}%
\contentsline {section}{\numberline {4.2}I\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Flying Class Reference}{8}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Member Function Documentation}{10}{subsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.1.1}fly()}{10}{subsubsection.4.2.1.1}%
\contentsline {subsubsection}{\numberline {4.2.1.2}land()}{10}{subsubsection.4.2.1.2}%
\contentsline {subsubsection}{\numberline {4.2.1.3}walkAround()}{11}{subsubsection.4.2.1.3}%
\contentsline {section}{\numberline {4.3}I\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Not\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Flying Class Reference}{11}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Member Function Documentation}{12}{subsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.1.1}jumpAround()}{12}{subsubsection.4.3.1.1}%
\contentsline {subsubsection}{\numberline {4.3.1.2}walkAround()}{12}{subsubsection.4.3.1.2}%
\contentsline {section}{\numberline {4.4}Penguin Class Reference}{13}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Member Function Documentation}{14}{subsection.4.4.1}%
\contentsline {subsubsection}{\numberline {4.4.1.1}jumpAround()}{14}{subsubsection.4.4.1.1}%
\contentsline {subsubsection}{\numberline {4.4.1.2}walkAround()}{15}{subsubsection.4.4.1.2}%
\contentsline {section}{\numberline {4.5}Raven Class Reference}{15}{section.4.5}%
\contentsline {subsection}{\numberline {4.5.1}Member Function Documentation}{16}{subsection.4.5.1}%
\contentsline {subsubsection}{\numberline {4.5.1.1}fly()}{16}{subsubsection.4.5.1.1}%
\contentsline {subsubsection}{\numberline {4.5.1.2}land()}{17}{subsubsection.4.5.1.2}%
\contentsline {subsubsection}{\numberline {4.5.1.3}walkAround()}{17}{subsubsection.4.5.1.3}%
\contentsline {chapter}{\numberline {5}File Documentation}{19}{chapter.5}%
\contentsline {section}{\numberline {5.1}Bird.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}cpp File Reference}{19}{section.5.1}%
\contentsline {section}{\numberline {5.2}Bird.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}h File Reference}{19}{section.5.2}%
\contentsline {section}{\numberline {5.3}I\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Flying.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}cpp File Reference}{20}{section.5.3}%
\contentsline {section}{\numberline {5.4}I\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Flying.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}h File Reference}{21}{section.5.4}%
\contentsline {section}{\numberline {5.5}I\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Not\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Flying.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}cpp File Reference}{22}{section.5.5}%
\contentsline {section}{\numberline {5.6}I\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Not\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Flying.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}h File Reference}{23}{section.5.6}%
\contentsline {section}{\numberline {5.7}Penguin.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}cpp File Reference}{24}{section.5.7}%
\contentsline {section}{\numberline {5.8}Penguin.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}h File Reference}{24}{section.5.8}%
\contentsline {section}{\numberline {5.9}Raven.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}cpp File Reference}{25}{section.5.9}%
\contentsline {section}{\numberline {5.10}Raven.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}h File Reference}{26}{section.5.10}%
\contentsline {chapter}{Index}{29}{section*.36}%
