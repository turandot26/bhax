#include <exception>
using namespace std;

#ifndef __INotFlying_h__
#define __INotFlying_h__

#include "Bird.h"

// class Bird;
class INotFlying;

__abstract class INotFlying: public Bird
{

	public: virtual bool walkAround() = 0;

	public: virtual bool jumpAround() = 0;
};

#endif
