#include <exception>
using namespace std;

#ifndef __Bird_h__
#define __Bird_h__

class Bird;

__abstract class Bird
{

	public: virtual bool walkAround() = 0;
};

#endif
