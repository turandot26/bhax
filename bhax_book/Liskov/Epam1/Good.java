    interface MyInterface{  
 
        default void newMethod(){  
            System.out.println("Newly added default method for MyInterface");  
        }  
        void existingMethod(String str);  
    }  
    interface MyInterface2{  
         
        default void newMethod(){  
            System.out.println("Newly added default method for MyInterface2");  
        }  
        void disp(String str);  
    } 
    public class Good implements MyInterface, MyInterface2{ 

        public void existingMethod(String str){           
            System.out.println("String is: "+str);  
        }  
        public void disp(String str){
            System.out.println("String is: "+str); 
        }
        @Override
        public void newMethod(){  
            System.out.println("Newly added implementation for the default methods of MyInterface and  MyInterface2");  
        }  
        
        public static void main(String[] args) {  
            Good obj = new Good();

            obj.newMethod();     
      
      
        }  
    }