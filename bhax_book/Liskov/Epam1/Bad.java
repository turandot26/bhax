    interface MyInterface{  
 
        default void newMethod(){  
            System.out.println("Newly added default method for MyInterface");  
        }  
        void existingMethod(String str);  
    }  
    interface MyInterface2{  
         
        default void newMethod(){  
            System.out.println("Newly added default method for MyInterface2");  
        }  
        void disp(String str);  
    } 
    public class Bad implements MyInterface, MyInterface2{ 

        public void existingMethod(String str){           
            System.out.println("String is: "+str);  
        }  
        public void disp(String str){
            System.out.println("String is: "+str); 
        }
        
        public static void main(String[] args) {  
            Bad obj = new Bad();

            obj.newMethod();     
      
      
        }  
    }