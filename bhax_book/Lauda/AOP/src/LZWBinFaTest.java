import static org.junit.Assert.*;

public class LZWBinFaTest {

    @org.junit.Test
    public void main() {
        LZWBinFa binfa = new LZWBinFa();
        String word = "101011101010000011110110";
        for(int i=0;i<word.length();i++) {
            binfa.egyBitFeldolg(word.charAt(i));
        }

        assertEquals(4, binfa.getMelyseg());
        assertEquals(3.0, binfa.getAtlag(), 0.0001);
        assertEquals(0.816496580927726, binfa.getSzoras(), 0.0001);
    }
}