import java.net.Socket;
import java.io.IOException;

public class PortScanner
{
    public static void main(String[] args)
    {
        if(args.length < 1)
        {
            System.err.println("Usage: java PortScanner <ip>");
            System.exit(-1);
        }
        
        Socket scannerSocket = null;
        int currentPort = 0;
        while(currentPort <= 1024)
        {
            try
            {
                scannerSocket = new Socket(args[0], currentPort);
                System.out.printf("!! A %d nyitva!!\n", currentPort);
            }
            catch(IOException e)
            {
                System.err.printf("!! A %d nincs nyitva!!\n", currentPort);
            }
            currentPort++;
                
        }
        
       
        
    }
}