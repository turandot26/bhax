public class Main {

    public static void main(String[] args) {
        IntCollection collection = new IntCollection(3);
        collection.push_back(0);
        collection.push_back(2);
        collection.push_back(1);
        System.out.println(collection);
        collection.sort();
        System.out.println(collection);
        System.out.println(collection.contains(0));
        System.out.println(collection.contains(1));
        System.out.println(collection.contains(2));
        System.out.println(collection.contains(3));
        System.out.println(collection.contains(4));
    }
}
