
import java.util.Arrays;

public class IntCollection {
    int[] arr;
    int index = 0;
    int capacity;
    boolean is_sorted = true;

    public IntCollection(int size){
        this.arr = new int[size];
        this.capacity = size;
    }

    public IntCollection(int[] array){
        this.arr = array;
        this.index = array.length;
        this.capacity = array.length;
        this.is_sorted = false;
    }

    public int[] sort(){
        int n = this.arr.length;
        for (int i = 0; i < n-1; ++i)
            for (int j = 0; j < n-i-1; ++j)
                if (this.arr[j] > this.arr[j+1]) {
                    int temp = this.arr[j];
                    this.arr[j] = this.arr[j+1];
                    this.arr[j+1] = temp;
                }
        this.is_sorted = true;
        return this.arr;
    }

    public void push_back(int num) {
        if (this.capacity <= this.index) {
            throw new IllegalArgumentException("The collection is full");
        }
        this.is_sorted = false;
        this.arr[this.index] = num;
        ++this.index;
    }

    public void push_back(int[] array){
        for(int i : array)
            this.push_back(i);
    }

    public boolean contains(int num) {
        if (!this.is_sorted)
            this.sort();

        int left = 0, right = this.arr.length - 1;
        while (left <= right) {
            int middle = left + (right - left) / 2;

            if (this.arr[middle] == num)
                return true;

            if (this.arr[middle] < num)
                left = middle + 1;
            else
                right = middle - 1;
        }

        return false;
    }

    @Override
    public String toString() {
        return "<[" + Arrays.toString(this.arr) + "]>";
    }

}
