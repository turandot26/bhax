import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import javax.swing.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Main {

    static class Program extends Frame implements ActionListener{
        TextField tf1,tf3;
        Button b1,b2,b3;
        Program(){
            addWindowListener(new AblakKezeles());
            tf1=new TextField();
            tf1.setBounds(50,50,150,20);

            tf3=new TextField();
            tf3.setBounds(50,150,150,20);
            tf3.setEditable(false);

            b1=new Button("Check");
            b1.setBounds(50,200,50,50);
            b2=new Button("Clear");
            b2.setBounds(120,200,50,50);
            b3=new Button("EXIT");
            b3.setBounds(190,200,50,50);

            b1.addActionListener(this);
            b2.addActionListener(this);
            b3.addActionListener(this);
            //addKeyListener(new BillentyuKezeles());
            
            add(tf1);add(tf3);add(b1);add(b2);add(b3);
            
            setSize(300,300);
            setLayout(null);
            setExtendedState(JFrame.MAXIMIZED_BOTH);
            setUndecorated(true);
            setVisible(true);
        }
        private byte[] getIP(String string) throws UnknownHostException {
            InetAddress IP = InetAddress.getByName(string);
            return IP.getAddress();
        }

        private String categorize(byte[] IP){
            if(Byte.compare(IP[0], (byte)192)==0 && Byte.compare(IP[1], (byte)168)==0 )
                return "Private";
            if (Byte.compare(IP[0], (byte)10)==0)
                return "Private";
            if(Byte.compare(IP[0], (byte)172)==0 && !(Byte.compare(IP[1], (byte)16)<0) && !(Byte.compare(IP[1], (byte)10)>0))
                return "Private";

            return "Public";
        }

        public void actionPerformed(ActionEvent e) {
            String s1=tf1.getText();

            String result="";

            if(e.getSource()==b1){
                try {
                    result = categorize(getIP(s1));
                } catch (UnknownHostException unknownHostException) {
                	result = "nothing appropriate";
                    unknownHostException.printStackTrace();
                }
            }else if(e.getSource()==b2){
                result = "";
                tf1.setText("");
            }else if(e.getSource() == b3){
            	System.exit(0);
            }

            tf3.setText(result);
        }

        class AblakKezeles extends WindowAdapter
        {
            public void windowClosing(WindowEvent e)
            {
                System.exit(0);
            }
        }
    }


    public static void main(String[] args) {
        new Program();
    }
}
