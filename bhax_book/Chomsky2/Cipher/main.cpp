#include <iostream>
#include <map>
#include <string>
#include <ctime>
#include <cstdlib>

class Encoder {
private:
    inline static const std::map<char, std::array<std::string, 4>> Encoding = {
            {'a', {"4", "4",    "@",      "/-\\"}},
            {'b', {"b", "8",    "|3",     "|}"}},
            {'c', {"c", "(",    "<",      "{"}},
            {'d', {"d", "|)",   "|]",     "|}"}},
            {'e', {"3", "3",    "3",      "3"}},
            {'f', {"f", "|=",   "ph",     "|#"}},
            {'g', {"g", "6",    "[",      "[+"}},
            {'h', {"h", "4",    "|-|",    "[-]"}},
            {'i', {"1", "1",    "|",      "!"}},
            {'j', {"j", "7",    "_|",     "_/"}},
            {'k', {"k", "|<",   "1<",     "|{"}},
            {'l', {"l", "1",    "|",      "|_"}},
            {'m', {"m", "44",   "(V)",    "|\\/|"}},
            {'n', {"n", "|\\|", "/\\/",   "/V"}},
            {'o', {"0", "0",    "()",     "[]"}},
            {'p', {"p", "/o",   "|D",     "|o"}},
            {'q', {"q", "9",    "O_",     "(,)"}},
            {'r', {"r", "12",   "12",     "|2"}},
            {'s', {"s", "5",    "$",      "$"}},
            {'t', {"t", "7",    "7",      "'|'"}},
            {'u', {"u", "|_|",  "(_)",    "[_]"}},
            {'v', {"v", "\\/",  "\\/",    "\\/"}},
            {'w', {"w", "VV",   "\\/\\/", "(/\\)"}},
            {'x', {"x", "%",    ")(",     ")("}},
            {'y', {"y", "y",     "y",       "y"}},
            {'z', {"z", "2",    "7_",     ">_"}},

            {'0', {"D", "0",    "D",      "0"}},
            {'1', {"I", "I",    "L",      "L"}},
            {'2', {"Z", "Z",    "Z",      "e"}},
            {'3', {"E", "E",    "E",      "E"}},
            {'4', {"h", "h",    "A",      "A"}},
            {'5', {"S", "S",    "S",      "S"}},
            {'6', {"b", "b",    "G",      "G"}},
            {'7', {"T", "T",    "j",      "j"}},
            {'8', {"X", "X",    "X",      "X"}},
            {'9', {"g", "g",    "j",      "j"}}
    };

    static std::string convertToLeet(char input){
        int randomIndex = rand()%4;

        auto keyValue_pair = Encoding.find(input);
        return keyValue_pair->second[randomIndex];
    }
public:
    static std::string getLeetSymbol(char input){
        char tmp = input;
        if(input >='A' && input<='Z')
            input += 32;

        if( (input>='a' && input <='z') || (input>='0' && input<='9')  )
            return convertToLeet(input);
        else {
            std::string s;
            s.push_back(tmp);
            s.push_back('\0');
            return s;
        }
    }
};

int main(){
    srand(time(nullptr));
    
    std::string buffer;
    while(std::getline(std::cin, buffer)){
        for(auto i: buffer)
            std::cout<<Encoder::getLeetSymbol(i);
		std::cout<<'\n';
	}
	
    std::cout <<std::endl;
}