#!/bin/zsh/

rm main lookup log.txt

clang++ main.cpp -std=c++20 -o main -O3 
clang++ lookup.cpp -o lookup -O3 

touch log.txt
echo "Map cipher with sample size: 1k bytes" >>log.txt
(time (./main <lorem_1k.txt >/dev/null)) 2>>log.txt
echo "Switch cipher with sample size: 1k bytes" >>log.txt
(time (./lookup <lorem_1k.txt >/dev/null)) 2>>log.txt

echo >>log.txt

echo "Map cipher with sample size: 10k bytes" >>log.txt
(time (./main <lorem_10k.txt >/dev/null)) 2>>log.txt
echo "Switch cipher with sample size: 10k bytes" >>log.txt
(time (./lookup <lorem_10k.txt >/dev/null)) 2>>log.txt

echo >>log.txt

echo "Map cipher with sample size: 100k bytes" >>log.txt
(time (./main <lorem_100k.txt >/dev/null)) 2>>log.txt
echo "Switch cipher with sample size: 100k bytes" >>log.txt
(time (./lookup <lorem_100k.txt >/dev/null)) 2>>log.txt

echo >>log.txt

echo "Map cipher with sample size: 1m bytes" >>log.txt
(time (./main <lorem_1m.txt >/dev/null)) 2>>log.txt
echo "Switch cipher with sample size: 1m bytes" >>log.txt
(time (./lookup <lorem_1m.txt >/dev/null)) 2>>log.txt

echo >>log.txt

echo "Map cipher with sample size: 10m bytes" >>log.txt
(time (./main <lorem_10m.txt >/dev/null)) 2>>log.txt
echo "Switch cipher with sample size: 10m bytes" >>log.txt
(time (./lookup <lorem_10m.txt >/dev/null)) 2>>log.txt

echo >>log.txt

echo "Map cipher with sample size: 100m bytes" >>log.txt
(time (./main <lorem_100m.txt >/dev/null)) 2>>log.txt
echo "Switch cipher with sample size: 100m bytes" >>log.txt
(time (./lookup <lorem_100m.txt >/dev/null)) 2>>log.txt
