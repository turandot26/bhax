#include <ctime>
#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>


void outToFile(std::string path, int size){
	std::ofstream outfile (path);

	for(int i = 0; i<size; ++i)
		outfile<< (char)(rand()%95+' ');

	outfile.close();
}
void outToStdout(int size){
	for(int i = 0; i<size; ++i)
		std::cout<< (char)(rand()%95+' ');

}
int main(int argc, char** argv){

	srand(time(nullptr));

	if(argc == 2){
		int size = atoi (argv[1]);
		outToStdout(size);
		exit(0);
	} else if(argc == 3){
		std::string destFileName (argv[2]);
		int size = atoi (argv[1]);
		outToFile(destFileName, size);
		exit(0);
	} else{
		std::cout<<"lipsum size [path]" <<std::endl;
		exit(1);
	} 
	
}