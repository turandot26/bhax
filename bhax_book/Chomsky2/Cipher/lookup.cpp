#include <iostream>
#include <string>
#include <array>
//#include "m_random.h"
#include <ctime>
#include <cstdlib>

class Encoder {
private:
	static std::string convertToLeet(char input){
		std::array<std::string, 4> leetStrings;
	
		switch(input){
			case 'A':
			case 'a':
				leetStrings = {"4", "4",    "@",      "/-\\"};
				break;
			case 'B':
			case 'b':
				leetStrings = {"b", "8",    "|3",     "|}"};
				break;
			case 'C':
			case 'c':
				leetStrings = {"c", "(",    "<",      "{"};
				break;
			case 'D':
			case 'd':
				leetStrings = {"d", "|)",   "|]",     "|}"};
				break;
			case 'E':
			case 'e':
				leetStrings = {"3", "3",    "3",      "3"};
				break;
			case 'F':
			case 'f':
				leetStrings = {"f", "|=",   "ph",     "|#"};
				break;
			case 'G':
			case 'g':
				leetStrings = {"g", "6",    "[",      "[+"};
				break;
			case 'H':
			case 'h':
				leetStrings = {"h", "4",    "|-|",    "[-]"};
				break;
			case 'I':
			case 'i':
				leetStrings = {"1", "1",    "|",      "!"};
				break;
			case 'J':
			case 'j':
				leetStrings = {"j", "7",    "_|",     "_/"};
				break;
			case 'K':
			case 'k':
				leetStrings = {"k", "|<",   "1<",     "|{"};
				break;
			case 'L':
			case 'l':
				leetStrings = {"l", "1",    "|",      "|_"};
				break;
			case 'M':
			case 'm':
				leetStrings = {"m", "44",   "(V)",    "|\\/|"};
				break;
			case 'N':
			case 'n':
				leetStrings = {"n", "|\\|", "/\\/",   "/V"};
				break;
			case 'O':
			case 'o':
				leetStrings = {"0", "0",    "()",     "[]"};
				break;
			case 'P':
			case 'p':
				leetStrings = {"p", "/o",   "|D",     "|o"};
				break;
			case 'Q':
			case 'q':
				leetStrings = {"q", "9",    "O_",     "(,)"};
				break;
			case 'R':
			case 'r':
				leetStrings = {"r", "12",   "12",     "|2"};
				break;
			case 'S':
			case 's':
				leetStrings = {"s", "5",    "$",      "$"};
				break;
			case 'T':
			case 't':
				leetStrings = {"t", "7",    "7",      "'|'"};
				break;
			case 'U':
			case 'u':
				leetStrings = {"u", "|_|",  "(_)",    "[_]"};
				break;
			case 'V':
			case 'v':
				leetStrings = {"v", "\\/",  "\\/",    "\\/"};
				break;
			case 'W':
			case 'w':
				leetStrings = {"w", "VV",   "\\/\\/", "(/\\)"};
				break;
			case 'X':
			case 'x':
				leetStrings = {"x", "%",    ")(",     ")("};
				break;
			case 'Y':
			case 'y':
				leetStrings = {"y", "",     "",       ""};
				break;
			case 'Z':
			case 'z':
				leetStrings = {"z", "2",    "7_",     ">_"};
				break;
			case '0':
				leetStrings = {"D", "0",    "D",      "0"};
				break;
			case '1':
				leetStrings = {"I", "I",    "L",      "L"};
				break;
			case '2':
				leetStrings = {"Z", "Z",    "Z",      "e"};
				break;
			case '3':
				leetStrings = {"E", "E",    "E",      "E"};
				break;
			case '4':
				leetStrings = {"h", "h",    "A",      "A"};
				break;
			case '5':
				leetStrings = {"S", "S",    "S",      "S"};
				break;
			case '6':
				leetStrings = {"b", "b",    "G",      "G"};
				break;
			case '7':
				leetStrings = {"T", "T",    "j",      "j"};
				break;
			case '8':
				leetStrings = {"X", "X",    "X",      "X"};
				break;
			case '9':
				leetStrings = {"g", "g",    "j",      "j"};
				break;
			default:
				std::string s;
            	s.push_back(input);
            	s.push_back('\0');
            	return s;
		}

		return leetStrings[rand()%4];

		
	}
    

public:
    static std::string getLeetSymbol(char input){
        return convertToLeet(input);
    }
};

int main(){
    srand(time(nullptr));
    
    std::string buffer;
    while(std::getline(std::cin, buffer)){
        for(auto i: buffer)
            std::cout<<Encoder::getLeetSymbol(i);
		std::cout<<'\n';
	}
	
    std::cout <<std::endl;

}